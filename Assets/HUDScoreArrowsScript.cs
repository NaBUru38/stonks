using UnityEngine;

public class HUDScoreArrowsScript : MonoBehaviour
{
    static GameObject arrowUpOn_Reddit;
    static GameObject arrowDownOn_Reddit;

    static GameObject arrowUpOn_Wolf;
    static GameObject arrowDownOn_Wolf;

    static CharacterCollissionScript stonks_Reddit;
    static CharacterCollissionScript stonks_Wolf;

    // Start is called before the first frame update
    void Start()
    {
        GameObject charactersObj = GameObject.Find ("/Characters");
        stonks_Reddit = charactersObj.transform.GetChild (0).GetComponent<CharacterCollissionScript> ();
        stonks_Wolf = charactersObj.transform.GetChild (1).GetComponent<CharacterCollissionScript> ();

        GameObject shudArrows_Reddit = GameObject.Find ("/Canvas/HUD Arrows Weeb");
        GameObject shudArrows_Wolf = GameObject.Find ("/Canvas/HUD Arrows Wolf");

        arrowUpOn_Reddit = shudArrows_Reddit.transform.GetChild (0).gameObject;
        arrowDownOn_Reddit = shudArrows_Reddit.transform.GetChild (2).gameObject;

        arrowUpOn_Wolf = shudArrows_Wolf.transform.GetChild (0).gameObject;
        arrowDownOn_Wolf = shudArrows_Wolf.transform.GetChild (2).gameObject;
    }

    public static void UpdateBuffs ()
    {
        if (stonks_Reddit.stonks > stonks_Wolf.stonks)
        {
            arrowUpOn_Reddit.SetActive (true);
            arrowDownOn_Reddit.SetActive (false);

            arrowUpOn_Wolf.SetActive (false);
            arrowDownOn_Wolf.SetActive (true);
        }

        else if (stonks_Reddit.stonks < stonks_Wolf.stonks)
        {
            arrowUpOn_Reddit.SetActive (false);
            arrowDownOn_Reddit.SetActive (true);

            arrowUpOn_Wolf.SetActive (true);
            arrowDownOn_Wolf.SetActive (false);
        }

        else
        {
            arrowUpOn_Reddit.SetActive (false);
            arrowDownOn_Reddit.SetActive (false);

            arrowUpOn_Wolf.SetActive (false);
            arrowDownOn_Wolf.SetActive (false);
        }
    }
}
