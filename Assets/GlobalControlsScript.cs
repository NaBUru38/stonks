using TMPro;
using UnityEngine;

public class GlobalControlsScript : MonoBehaviour
{
    public float secondsPerMatch = 140;

    private float countdownTime;

    TextMeshPro countdownTextMesh;

    GameObject menusObj;

    GameObject charactersObj;

    GameObject start;
    GameObject credits;
    GameObject exit;

    Sprite startIn;
    Sprite creditsIn;
    Sprite exitIn;

    Sprite startAct;
    Sprite creditsAct;
    Sprite exitAct;

    bool isGameplayRunning;
    bool isMainMenu;
    bool isEndgameMenu;
    bool isCreditMenu;
    bool isInstructionsMenu;

    int buttonIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameObject countdownObj = GameObject.Find ("/Canvas/Countdown");
        menusObj = GameObject.Find ("/Menus");
        charactersObj = GameObject.Find ("/Characters");

        start = GameObject.Find ("/Menus/Main Menu/Start");
        credits = GameObject.Find ("/Menus/Main Menu/Credits");
        exit = GameObject.Find ("/Menus/Main Menu/Exit");

        startIn = Resources.Load ("Comenzar-in", typeof (Sprite)) as Sprite;
        creditsIn = Resources.Load ("Creditos-in", typeof (Sprite)) as Sprite;
        exitIn = Resources.Load ("Salir-in", typeof (Sprite)) as Sprite;

        startAct = Resources.Load ("Comenzar-act", typeof (Sprite)) as Sprite;
        creditsAct = Resources.Load ("Creditos-act", typeof (Sprite)) as Sprite;
        exitAct = Resources.Load ("Salir-act", typeof (Sprite)) as Sprite;


        if (countdownObj)
        {
            countdownTextMesh = countdownObj.GetComponent<TextMeshPro> ();
        }

        // show main menu
        LoadMainMenu ();

        countdownTime = -1;
    }

    void LoadMainMenu ()
    {
        HideMenus (true);
        isMainMenu = true;

        menusObj.transform.Find ("Main Menu").gameObject.SetActive (true);
    }

    void EndGame()
    {
        GameObject playerA = charactersObj.transform.GetChild (0).gameObject;
        GameObject playerB = charactersObj.transform.GetChild (1).gameObject;
        int stonksA = playerA.GetComponent<CharacterCollissionScript>().stonks;
        int stonksB = playerB.GetComponent<CharacterCollissionScript>().stonks;

        if (stonksA > stonksB)
        {
            CharacterCollissionScript infoPlayerA = playerA.GetComponent<CharacterCollissionScript> ();
            showHasWon (infoPlayerA);
        }
        else if(stonksB > stonksA)
        {
            CharacterCollissionScript infoPlayerB = playerB.GetComponent<CharacterCollissionScript> ();
            showHasWon (infoPlayerB);
        }
        else
        {
            Debug.Log("Overtime!");

            countdownTime = 10;
        }
    }

    void showHasWon(CharacterCollissionScript infoPlayer)
    {
        HideMenus (false);
        isEndgameMenu = true;

        if (infoPlayer.isWolf)
        {
            Debug.Log("Wolf has won.");

            menusObj.transform.Find ("Wolf Won").gameObject.SetActive (true);

            AudioEffectsScript.playSound (SFX_Type.SFX_WinWolf);
        }
        else
        {
            Debug.Log("Weeb has won.");

            menusObj.transform.Find ("Weeb Won").gameObject.SetActive (true);

            AudioEffectsScript.playSound (SFX_Type.SFX_WinWeeb);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!menusObj)
        {
            return;
        }

        if (isGameplayRunning)
        {
            countdownTime -= Time.deltaTime;

            if (countdownTextMesh)
            {
                countdownTextMesh.text = (Mathf.CeilToInt (countdownTime)).ToString ();
            }

            //Debug.Log("seconds left: " + Mathf.FloorToInt (countdownTime));

            if (countdownTime < 0)
            {
                EndGame ();
            }
        }

        if(isMainMenu)
        {
            if(Input.GetKeyDown(KeyCode.DownArrow))
            {
                buttonIndex++;
            } else if(Input.GetKeyDown(KeyCode.UpArrow))
            {
                buttonIndex--;
            } 

            if(buttonIndex > 2)
            {
                buttonIndex = 0;
            } else if(buttonIndex < 0)
            {
                buttonIndex = 2;
            }

            switch (buttonIndex)
            {
                case 0:
                    start.GetComponent<SpriteRenderer>().sprite = startAct;

                    credits.GetComponent<SpriteRenderer>().sprite = creditsIn;
                    exit.GetComponent<SpriteRenderer>().sprite = exitIn;
                    break;
                case 1:
                    credits.GetComponent<SpriteRenderer>().sprite = creditsAct;
                    
                    start.GetComponent<SpriteRenderer>().sprite = startIn;
                    exit.GetComponent<SpriteRenderer>().sprite = exitIn;
                    break;
                case 2:
                    exit.GetComponent<SpriteRenderer>().sprite = exitAct;

                    start.GetComponent<SpriteRenderer>().sprite = startIn;
                    credits.GetComponent<SpriteRenderer>().sprite = creditsIn;

                    break;
            }
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            Restart();
        } else if(Input.GetKeyDown(KeyCode.Escape) && ! isMainMenu)
        {
            LoadMainMenu();
        }

        if (isCreditMenu && Input.anyKeyDown)
        {
            LoadMainMenu();
        }


        if (! isGameplayRunning && (Input.GetKeyDown (KeyCode.Return) || Input.GetKeyDown (KeyCode.Space)))
        { 

            if (isMainMenu)
            {
                switch (buttonIndex)
                {
                    // continu
                    case 0:
                        HideMenus(true);
                        isInstructionsMenu = true;
                        menusObj.transform.Find ("Instructions Menu").gameObject.SetActive (true);
                        break;
                    case 1:
                        ShowCredits();
                        break;
                    case 2:
                        Application.Quit();
                        break;
                }
            }
            else if (isEndgameMenu)
            {
                LoadMainMenu ();
            }
            else if (isInstructionsMenu)
            {
                Restart ();
            }
        }
    }

    void ShowCredits()
    {
        HideMenus(true);

        menusObj.transform.Find("Credits Menu").gameObject.SetActive(true);
        isCreditMenu = true;
    }

    void Restart()
    {
        HideMenus (false);

        // restart the players
        bool isSpawnOnTop = Random.value < 0.5f;
        foreach (Transform characterTransf in charactersObj.transform)
        {
            characterTransf.gameObject.SetActive (true);
            characterTransf.GetComponent<CharacterCollissionScript> ().restart (isSpawnOnTop);
        }

        // activate one stink
        StonkScript.ActivateFirstStonk ();

        // start the countdown
        countdownTime = secondsPerMatch;
        isGameplayRunning = true;

        HUDScoreArrowsScript.UpdateBuffs ();

        AudioEffectsScript.playSound (SFX_Type.SFX_Music);

        Debug.Log ("Let's S_T_O_N_K_S !!!");
    }

    void HideMenus (bool playKilk)
    {
        isGameplayRunning = false;
        isMainMenu = false;
        isEndgameMenu = false;
        isCreditMenu = false;
        isInstructionsMenu = false;

        if (playKilk)
        {
            AudioEffectsScript.playSound (SFX_Type.SFX_Klik);
        }

        foreach (Transform menuTransf in menusObj.transform)
        {
            menuTransf.gameObject.SetActive (false);
        }

        foreach (Transform characterTransf in charactersObj.transform)
        {
            characterTransf.gameObject.SetActive (false);
        }
    }
}
