using UnityEngine;

public class CharacterControlsScript : MonoBehaviour
{
    // public parameters

    public int nJumpForce = 4000;
    public int nMoveForce = 8;

    public int playerNum;

    // private parameters

    bool isJumping;

    string inputHorizontalName, inputJumpName;

    // gameobjects, transforms and scripts

    Rigidbody2D mRigidbody;
    private Collider2D coll;

    private enum State {idle,running,jumping,falling}
    private State state = State.idle;

    [SerializeField] private LayerMask ground;

    private Animator normalAnim, furryAnim;

    CharacterGroundCheckScript groundCheckScript;

    // Start is called before the first frame update
    void Start()
    {
        GameObject normalAnimObj = transform.Find ("RegularAnimation").gameObject;
        GameObject furryAnimObj = transform.Find ("FurryAnimation").gameObject;

        normalAnim = normalAnimObj.GetComponent<Animator> ();
        furryAnim = furryAnimObj.GetComponent<Animator> ();

        groundCheckScript =transform.GetChild(0).GetComponent<CharacterGroundCheckScript>();
        coll=GetComponent<Collider2D>();

        isJumping = false;

        playerNum = transform.GetSiblingIndex ();

        mRigidbody = transform.GetComponent<Rigidbody2D> ();

        inputHorizontalName = "P" + (playerNum + 1) + "_MoveHorizontal";
        inputJumpName = "P" + (playerNum + 1) + "_MoveJump";
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float inputHorizontal = Input.GetAxis (inputHorizontalName);
        bool inputJump = Input.GetButton (inputJumpName);

        if (inputJump && !isJumping)
        {
            if (groundCheckScript.isOnGround)
            {
                state=State.jumping;
                mRigidbody.AddForce (new Vector2 (0.0f, nJumpForce));
            }
        }

        if (inputHorizontal > 0.2f)
        {
            mRigidbody.velocity = new Vector2 (nMoveForce, mRigidbody.velocity.y);
            transform.localScale = new Vector3(1,1,1);
        }
        else if (inputHorizontal < -0.2f)
        {
            mRigidbody.velocity = new Vector2 (-nMoveForce, mRigidbody.velocity.y);
            transform.localScale = new Vector3(-1,1,1);
        }

        CheckTeleportals ();

        UpdateVelocityState();
        //Debug.Log(gameObject.name + " / state: " + state);
        normalAnim.SetInteger ("state", (int)state);
        furryAnim.SetInteger ("state", (int)state);
    }

    void UpdateVelocityState ()
    {
        if (state == State.jumping) {
            if (mRigidbody.velocity.y < -0.1f) {
                state = State.falling;
            }

        } else if (state == State.falling) {
            if (groundCheckScript.isOnGround) {
                state = State.idle;
            }
        }
        else if (Mathf.Abs (mRigidbody.velocity.x) > 0.1) {
            state = State.running;
        } else {
            state = State.idle;
        }

    }

    void CheckTeleportals()
    {
        // Teleport through the portals on the right
        if (transform.position.x > 12.0f)
        {
            Vector3 newPosition = transform.position;
            newPosition.x -= 24.0f;
            if (transform.position.y > 3.0f)
            {
                newPosition.y -= 9.5f;
            }
            else
            {
                newPosition.y += 10.5f;
            }
            transform.position = newPosition;
        }

        // Teleport through the portals on the left
        else if (transform.position.x < - 12.0f)
        {
            Vector3 newPosition = transform.position;
            newPosition.x += 24.0f;
            if (transform.position.y > 3.0f)
            {
                newPosition.y -= 9.5f;
            }
            else
            {
                newPosition.y += 10.5f;
            }
            transform.position = newPosition;
        }
    }
}
