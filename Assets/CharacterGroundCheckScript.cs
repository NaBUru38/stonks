using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterGroundCheckScript : MonoBehaviour
{
    public bool isOnGround;
    // Start is called before the first frame update
    void Start()
    {
        isOnGround=false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D (Collider2D col)
    {        
        if (col.gameObject.tag == "Ground")
        {
            isOnGround=true;

        }
        
    }

    void OnTriggerExit2D (Collider2D col)
    {        
        if (col.gameObject.tag == "Ground")
        {
            isOnGround=false;

        }
        
    }
}
