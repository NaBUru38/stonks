using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSkin : MonoBehaviour
{
    public AnimatorOverrideController weebAnim;
    public AnimatorOverrideController wolfAnim;

    public void WolfSkin(){
        GetComponent<AnimatorOverrideController>().runtimeAnimatorController = wolfAnim as RuntimeAnimatorController;
    }

    public void WeebSkin(){
        GetComponent<AnimatorOverrideController>().runtimeAnimatorController = weebAnim as RuntimeAnimatorController;
    }
}
