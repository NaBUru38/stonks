using UnityEngine;

public enum SFX_Type
{
    SFX_Klik,
    SFX_WinWeeb,
    SFX_WinWolf,
    SFX_Lose,
    SFX_Music,
    SFX_Pickup,
    SFX_FurryPlay,
    SFX_FurryStop,
}

public class AudioEffectsScript : MonoBehaviour
{
    static AudioEffectsScript theInstance;

    static AudioClip sfxWinWeeb, sfxWinWolf, sfxMusic;
    static AudioClip sfxKlik, sfxPickup, sfxFurry, sfxLose;

    static AudioSource sfxAudioSource, musicAudioSource, powerupLoopAudioSource;

    static void Initialize ()
    {
        sfxKlik = Resources.Load<AudioClip> ("Sounds/menuClick");
        sfxWinWeeb = Resources.Load<AudioClip> ("Sounds/TimeUpWeebsVictory");
        sfxWinWolf = Resources.Load<AudioClip> ("Sounds/TimeUpWolvesVictory");
        sfxPickup = Resources.Load<AudioClip> ("Sounds/AgarrarStonk");
        sfxLose = Resources.Load<AudioClip> ("Sounds/perderStonk");
        sfxMusic = Resources.Load<AudioClip> ("Sounds/mainThemeFinal");
    }

    void Update ()
    {
        if (powerupLoopAudioSource.isPlaying && powerupLoopAudioSource.time > 0.8f)
        {
            powerupLoopAudioSource.Play ();
        }
    }

    public static void playSound (SFX_Type sfxType)
    {
        if (! theInstance)
        {
            Initialize ();
        }

        sfxAudioSource = GameObject.Find("/Singleton/SFX").GetComponent<AudioSource> ();
        musicAudioSource = GameObject.Find ("/Singleton/Music").GetComponent<AudioSource> ();
        powerupLoopAudioSource = GameObject.Find ("/Singleton/Powerup Loop").GetComponent<AudioSource> ();

        AudioClip sfxClip = null;

        switch (sfxType)
        {
            case SFX_Type.SFX_Klik:
                sfxClip = sfxKlik;
                break;
            case SFX_Type.SFX_WinWeeb:
                sfxClip = sfxWinWeeb;
                break;
            case SFX_Type.SFX_WinWolf:
                sfxClip = sfxWinWolf;
                break;
            case SFX_Type.SFX_Pickup:
                sfxClip = sfxPickup;
                break;
            case SFX_Type.SFX_Lose:
                sfxClip = sfxLose;
                break;
            case SFX_Type.SFX_Music:
                sfxClip = sfxMusic;
                break;
            default:
                break;
        }

        //Debug.Log ("let's play sound!: " + sfxClip.name);

        if (sfxType == SFX_Type.SFX_FurryPlay)
        {
            powerupLoopAudioSource.Play ();
        }
        else if (sfxType == SFX_Type.SFX_FurryStop)
        {
            powerupLoopAudioSource.Stop ();
        }
        else if (sfxType == SFX_Type.SFX_Music || sfxType == SFX_Type.SFX_WinWeeb || sfxType == SFX_Type.SFX_WinWolf)
        {
            musicAudioSource.Stop ();
            musicAudioSource.PlayOneShot (sfxClip);
        }
        else
        {
            sfxAudioSource.PlayOneShot (sfxClip);
        }
    }
}
