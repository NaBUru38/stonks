using UnityEngine;
using TMPro;

public class CharacterCollissionScript : MonoBehaviour
{
    // static parameters

    static int spawnIndex = -1;

    // public parameters

    public bool isWolf = false;

    public int stonks;

    public int superpowerDuration = 5;

    public float superpowerTimer;

    // private parameters

    int superpowerCount;

    bool isSuperpower;

    // gameobjects, transforms and scripts

    Transform respawnPoints;

    Rigidbody2D theRigidBody;

    private Animator buffAnim;

    private Animator debuffAnim;

    TextMeshPro textMesh;

    GameObject normalAnimObj, furryAnimObj;

    // Start is called before the first frame update
    void Start ()
    {
    }

    void Initialize ()
    {
        buffAnim = transform.GetChild(1).GetComponent<Animator>();
        debuffAnim = transform.GetChild(2).GetComponent<Animator>();

        normalAnimObj = transform.Find ("RegularAnimation").gameObject;
        furryAnimObj = transform.Find ("FurryAnimation").gameObject;

        theRigidBody = transform.GetComponent<Rigidbody2D> ();

        respawnPoints = GameObject.Find ("/Respawn Points").transform;

        if (spawnIndex < 0)
        {
            spawnIndex = Random.Range (0, respawnPoints.childCount);
        }

        if (isWolf)
        {
            textMesh = GameObject.Find ("/Canvas/HUD Stonks Wolf").GetComponent<TextMeshPro> ();
        }
        else
        {
            textMesh = GameObject.Find ("/Canvas/HUD Stonks Weeb").GetComponent<TextMeshPro> ();
        }

        restart (true);
    }

    void Update ()
    {
        // If the timer counts down to zero, stop the superpower.
        if (superpowerTimer > 0.0f)
        {
            superpowerTimer -= Time.deltaTime;

            if (superpowerTimer <= 0.0f)
            {
                isSuperpower = false;

                normalAnimObj.SetActive (true);
                furryAnimObj.SetActive (false);

                AudioEffectsScript.playSound (SFX_Type.SFX_FurryStop);

                Debug.Log (gameObject.name + " lost superpowers");
            }
        }

        if(isSuperpower){
            transform.GetComponent<ChangeSkin>().WolfSkin();
        }
    }

    public void restart (bool isTop)
    {
        if (!theRigidBody)
        {
            Initialize ();
        }

        stonks = 25;
        printStonks ();

        superpowerCount = 0;
        isSuperpower = false;

        normalAnimObj.SetActive (true);
        furryAnimObj.SetActive (false);

        // respawn on left if weeb, on right if wolf.
        int spawnIndex = 0;
        if (! isTop)
        {
            spawnIndex += 2;
        }
        if (isWolf)
        {
            spawnIndex += 1;
        }

        Transform respownPoint = respawnPoints.GetChild (spawnIndex);

        // Respawn at the spawn position
        transform.position = respownPoint.position;
        theRigidBody.velocity = Vector3.zero;
    }

    public void AddStonks()
    {
        AudioEffectsScript.playSound (SFX_Type.SFX_Pickup);

        buffAnim.SetBool("Buff",true);
        buffAnim.Play("Buff", -1, 0f);
        
        stonks += Random.Range(3, 6);

        printStonks ();

        // If a weeb collects 3 superpowers, get superpowers.
        superpowerCount++;
        if (!isWolf && superpowerCount == 3)
        {
            isSuperpower = true;
            superpowerCount = 0;
            superpowerTimer = superpowerDuration;

            normalAnimObj.SetActive (false);
            furryAnimObj.SetActive (true);

            AudioEffectsScript.playSound (SFX_Type.SFX_FurryPlay);

            Debug.Log (gameObject.name + " got superpowers");
        }
    }

    public void respawn ()
    {
        // Get a random spawn position
        spawnIndex = (spawnIndex + Random.Range(1, respawnPoints.childCount)) % respawnPoints.childCount;
        Transform respownPoint = respawnPoints.GetChild (spawnIndex);

        // Respawn at the spawn position
        transform.position = respownPoint.position;
        theRigidBody.velocity = Vector3.zero;
    }

    // Executes when the the character starts colliding with another object.
    void OnCollisionEnter2D (Collision2D col)
    {
        CharacterCollissionScript otherCollssionScript = col.gameObject.GetComponent<CharacterCollissionScript> ();

        // If I'm a weeb and touched a wolf...
        if (otherCollssionScript && otherCollssionScript.isWolf && !isWolf )
        {
            Debug.Log (gameObject.name + " / is other wolf: " + otherCollssionScript.isWolf);

            AudioEffectsScript.playSound (SFX_Type.SFX_Lose);

            // If I have superpower, attack the wolf.
            if (isSuperpower)
            {
                int stolenStonks = otherCollssionScript.loseStonks();
                stealsStonks(stolenStonks);
                Debug.Log("super weeb steals "+stolenStonks);
            }

            // If I don't have superpower, get attacked by the wolf.
            else
            {
                int lostStonks = loseStonks();
                otherCollssionScript.stealsStonks(lostStonks);
                Debug.Log("normal weeb looses "+lostStonks);
            }
        }
    }

    /// This character loses stonks and respawns.
    int loseStonks()
    {
        debuffAnim.SetBool("Debuff",true);
        debuffAnim.Play("Debuff", -1, 0f);

        int stolenStonks = Mathf.Max(stonks/5, 2);
        stonks -= stolenStonks;

        printStonks ();

        respawn ();

        //TODO: si stonks <= 0, perder la partida.

        return stolenStonks;
    }

    /// This character steals stonks.
    void stealsStonks (int stolenStonks)
    {
        buffAnim.SetBool("Buff",true);
        buffAnim.Play("Buff", -1, 0f);

        stonks += stolenStonks;

        printStonks ();
    }

    void printStonks ()
    {
        if (textMesh)
        {
            textMesh.text = stonks.ToString ();
        }

        HUDScoreArrowsScript.UpdateBuffs ();
    }
}
