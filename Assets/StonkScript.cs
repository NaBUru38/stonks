using UnityEngine;

public class StonkScript : MonoBehaviour
{
    // static parameters

    public static Transform stonksTransformer;

    static int spawnIndex = -1;

    // Start is called before the first frame update
    void Start()
    {
        if (! stonksTransformer)
        {
            stonksTransformer = GameObject.Find ("/Stonks").transform;
        }

        if (stonksTransformer && spawnIndex < 0)
        {
            spawnIndex = Random.Range (0, stonksTransformer.childCount);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Activate()
    {
        gameObject.SetActive(true);
    }

    private void Disable()
    {
        gameObject.SetActive(false);
    }

    void OnTriggerEnter2D (Collider2D collision)
    {
        CharacterCollissionScript otherCollssionScript = collision.gameObject.GetComponent<CharacterCollissionScript>();
        if (otherCollssionScript)
        {
            otherCollssionScript.AddStonks();

            Disable();
            ActivateStonk();
        }
    }

    /// Activate a random stonk, but not from the center of the screen.
    public static void ActivateFirstStonk ()
    {
        if (! stonksTransformer)
        {
            stonksTransformer = GameObject.Find ("/Stonks").transform;
        }

        foreach (Transform stonk in stonksTransformer)
        {
            stonk.GetComponent<StonkScript> ().Disable ();
        }

        spawnIndex = Random.Range (3, stonksTransformer.childCount);
        stonksTransformer.GetChild (spawnIndex).gameObject.GetComponent<StonkScript> ().Activate ();
    }

    /// Activate a random stonk, different from the previous one.
    static void ActivateStonk()
    {
        if (! stonksTransformer)
        {
            stonksTransformer = GameObject.Find("/Stonks").transform;
        }

        spawnIndex = (spawnIndex + Random.Range (1, stonksTransformer.childCount)) % stonksTransformer.childCount;
        stonksTransformer.GetChild(spawnIndex).gameObject.GetComponent<StonkScript> ().Activate ();
    }
}
