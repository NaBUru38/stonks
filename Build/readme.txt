
S T O N K S: Weebs & Wolves

Global Game Jam Uruguay 2021

https://globalgamejam.org/2021/games/s-t-o-n-k-s-weebs-wolves-8

https://bitbucket.org/NaBUru38/stonks

- o -

Description

Stock market used to be a thing for the wealthy ones.
Until some weebs appeared and proved that wrong.

Fight the wolves and see who gets more "stonks"
by the end of the round, in this super fun platform battle game.

Find the balance that has been lost for a long time.

- o -

Credits

* Programming: Vicente Bermúdez, Ignacio Bettosini, Belén Vignolo.
* Art and Animation: Iván Dominguez, Franco Ermolli, Mattia Ermolli, Valeria Sarro.
* Music and SFX: Valeria Sarro.

- o -

Licence

https://creativecommons.org/licenses/by-nc-sa/4.0/

Creative Commons Attribution-NonCommercial-ShareAlike
